# Clustering Crypto

In this project, I put unsupervised-learning and Amazon SageMaker skills into action by clustering cryptocurrencies and creating plots to present my results by generating a report of what cryptocurrencies are available on the trading market and how they can be grouped using classification.

# Main Tasks 

- Data Preprocessing: Prepare data for dimension reduction with PCA and clustering using K-Means.
- Reducing Data Dimensions Using PCA: Reduce data dimension using the PCA algorithm from sklearn.
- Clustering Cryptocurrencies Using K-Means: Predict clusters using the cryptocurrencies data using the KMeans algorithm from sklearn.
- Visualizing Results: Create some plots and data tables to present your results.
- AWS Sagemaker: Deploy your notebook to Amazon SageMaker.

# Instructions

## Data Preprocessing
In this section, you will load the information about cryptocurrencies and perform data preprocessing tasks.

With the data loaded into a Pandas DataFrame, continue with the following data preprocessing tasks.

- Keep only the necessary columns: 'CoinName','Algorithm','IsTrading','ProofType','TotalCoinsMined','TotalCoinSupply'
- Keep only the cryptocurrencies that are trading.
- Keep only the cryptocurrencies with a working algorithm.
- Remove the IsTrading column.
- Remove all cryptocurrencies with at least one null value.
- Remove all cryptocurrencies that have no coins mined.
- Drop all rows where there are 'N/A' text values.
- Store the names of all cryptocurrencies in a DataFrame named coins_name, use the crypto_df.index as the index for this new DataFrame.
- Remove the CoinName column.
- Create dummy variables for all the text features, and store the resulting data in a DataFrame named X.
- Use the StandardScaler from sklearn to standardize all the data of the X DataFrame. Remember, this is important prior to using PCA and K-Means algorithms.

## Reducing Data Dimensions Using PCA

Use the PCA algorithm from sklearn to reduce the dimensions of the X DataFrame down to three principal components.

## Clustering Cryptocurrencies Using K-Means

In this section, I have used the KMeans algorithm from sklearn to cluster the cryptocurrencies using the PCA data.

## Visualizing Results
In this section, I have created some data visualization to present the final results. Perform the following tasks:
- Create a 3D-Scatter using Plotly Express to plot the clusters using the clustered_df DataFrame.
- Use hvplot.table to create a data table with all the current tradable cryptocurrencies. 
- Create a scatter plot using hvplot.scatter, to present the clustered data about cryptocurrencies having x="TotalCoinsMined" and y="TotalCoinSupply" to contrast the number of available coins versus the total number of mined coins.


## AWS Sagemaker

I have uploaded my Jupyter notebook to Amazon SageMaker and deployed it.

The hvplot and Plotly Express libraries are not included in the built-in anaconda environments, so for this challenge section, I used the altair library instead.

